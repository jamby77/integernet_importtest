<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Contacts
 * @copyright   Copyright (c) 2013 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Contacts index controller
 *
 * @category   Mage
 * @package    Mage_Contacts
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class IntegerNet_ImportTests_IndexController extends Mage_Core_Controller_Front_Action
{

    public function deletesessionAction()
    {
        $session = Mage::getSingleton('core/session');
        $session->unsConsumerKey();
        $session->unsConsumerSecret();
        $session->unsOauthToken();
        $session->setState(0);
        session_destroy();
        $this->_redirect("getaccesstoken");
    }

    public function indexAction()
    {
        if (!class_exists('OAuth')) {
            Mage::throwException('Please install the OAuth extension for PHP.');
        }
        Mage::app()->cleanCache();
        $error = false;
        $translate = Mage::getSingleton('core/translate');
        $baseUri = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
        $callbackUrl = $baseUri.'getaccesstoken';
        $temporaryCredentialsRequestUrl = $baseUri.'oauth/initiate?oauth_callback=' . urlencode($callbackUrl);
        $adminAuthorizationUrl = $baseUri.'admin/oauth_authorize';
        $accessTokenRequestUrl = $baseUri.'oauth/token';
        $apiUrl = $baseUri.'api/rest';
        $session = Mage::getSingleton('core/session');
        $setup = Mage::getModel('eav/entity_setup', 'core_setup');
        if($this->getRequest()->isPost())
        {
            $session->setConsumerKey($this->getRequest()->getPost('key'));
            echo "Consumerkey: ".$session->getConsumerKey()."<br />";
            $session->setConsumerSecret($this->getRequest()->getPost('secret'));
            $setup->setConfigData('ImportTest/ConsumerKey', $session->getConsumerKey());
            $setup->setConfigData('ImportTest/ConsumerSecret', $session->getConsumerSecret());
        }
        if(($consumerKey = $session->getConsumerKey()) && ($consumerSecret = $session->getConsumerSecret()))
        {
            try{
                if (!$this->getRequest()->getParam('oauth_token') && $session->getState() == 1) {
                    $session->setState(0);
                }
                $authType = ($session->getState() == 2) ? OAUTH_AUTH_TYPE_AUTHORIZATION : OAUTH_AUTH_TYPE_URI;
                $oauthClient = new OAuth($consumerKey, $consumerSecret, OAUTH_SIG_METHOD_HMACSHA1, $authType);
                $oauthClient->enableDebug();

                if (!$this->getRequest()->getParam('oauth_token') && !$session->getState()) {
                    $requestToken = $oauthClient->getRequestToken($temporaryCredentialsRequestUrl);
                    $session->setSecret($requestToken['oauth_token_secret']);
                    $session->setState(1);
                    header('Location: ' . $adminAuthorizationUrl . '?oauth_token=' . $requestToken['oauth_token']);
                    exit;
                } else if ($session->getState() == 1) {
                    $oauthClient->setToken($this->getRequest()->getParam('oauth_token'), $session->getSecret());
                    $accessToken = $oauthClient->getAccessToken($accessTokenRequestUrl);
                    $session->setState(2);
                    $session->setOauthToken($accessToken['oauth_token']);
                    $setup->setConfigData('ImportTest/OauthToken', $accessToken['oauth_token']);
                    $setup->setConfigData('ImportTest/OauthTokenSecret', $accessToken['oauth_token_secret']);
                    header('Location: ' . $callbackUrl);
                    exit;
                } else {
                    // Test Permissions
                    $oauthClient->setToken(Mage::getStoreConfig('ImportTest/OauthToken'), Mage::getStoreConfig('ImportTest/OauthTokenSecret'));
                    $headers = array('Content-Type' => 'application/json');
                    $stock_data = array(
                        'qty' => 20,
                        'is_in_stock' => 1,
                        'stock_id' => 1,
                        'store_id' => 1,
                        'manage_stock' => 1,
                        'use_config_manage_stock' => 0,
                        'min_sale_qty' => 0,
                        'use_config_min_sale_qty' => 0,
                        'max_sale_qty' => 1000,
                        'use_config_max_sale_qty' => 0,
                    );
                    $resourceUrl = "$apiUrl/products";
                    $productData = json_encode(array(
                        'attribute_set_id' => 4,
                        'type_id' => 'simple',
                        'sku' => 'importtestoauth'.uniqid(),
                        'name' => 'Test',
                        'description' => 'Das ist ein Test',
                        'short_description' => 'Testprodukt',
                        'weight' => 4,
                        'status' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED,
                        'visibility' => Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
                        'tax_class_id' => 0,
                        'price' => 20,
                        'stock_data' => $stock_data,
                    ));

                    @$oauthClient->fetch($resourceUrl, $productData, 'POST', $headers);

                    $resource = Mage::getSingleton('core/resource');
                    $write = $resource->getConnection(Mage_Core_Model_Resource::DEFAULT_SETUP_RESOURCE);
                    $write->query("DELETE FROM catalog_product_entity WHERE sku LIKE 'importtestoauth%'");
                    $collection = Mage::getModel('index/process')->getCollection();
                    foreach ($collection as $item) {
                        $item->reindexAll();
                    }
                }
            }
            catch(OAuthException $e)
            {
                $error = true;
                $translate->setTranslateInline(true);
                if($e->getCode() == '500') {
                    Mage::getSingleton('customer/session')->addError(Mage::helper('importtests')->__('Consumer does not have permission to add Products.'));
                }
                elseif($e->getCode() == '401') {
                    Mage::getSingleton('customer/session')->addError(Mage::helper('importtests')->__('Consumer key and/or secret are not valid.'));
                }
                elseif($e->getCode() == '400') {
                    Mage::getSingleton('customer/session')->addError(Mage::helper('importtests')->__('Rest is using Default attribute set with id 4. Make sure it exists and make sure there is no required attribute rather than sku, name, description, short_description, weight, status, visibility, tax class, price.'));
                }
                else {
                    print_r($e);
                }
            }
        }

        if($session->getOauthToken() && !$error) {
            $translate->setTranslateInline(true);

            Mage::getSingleton('customer/session')->addSuccess(Mage::helper('importtests')->__('You are ready to use Rest.'));
        }

        $this->loadLayout();
        $block = $this->getLayout()->createBlock('core/template','integernet.getaccesstoken')->setTemplate('integernet/getaccesstoken.phtml');
        $block->setError($error);
        $this->getLayout()->getBlock('content')->append($block);
        $this->_initLayoutMessages('customer/session');
        $this->renderLayout();
    }

}
