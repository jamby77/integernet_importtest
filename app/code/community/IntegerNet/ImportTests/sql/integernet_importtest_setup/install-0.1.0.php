<?php
/**
* integer_net Magento Module
*
* @category   IntegerNet
* @package    IntegerNet_ImportTest
* @copyright  Copyright (coffee) 2013 integer_net GmbH (http://www.integer-net.de/)
* @author     Soeren Zorn <sz@integer-net.de>
*/

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->addAttribute('catalog_product', 'importtest_configurable', array(
    'label'             => 'ImportTest-Configure-Input',
    'type'              => 'varchar',
    'input'             => 'select',
    'required'          => 1,
    'user_defined'      => 1,
    'group'             => 'General',
    'global'            => 1,
    'visible'           => 1,
    'searchable'        => 0,
    'filterable'        => 0,
    'filterable_in_search' => 0,
    'visible_on_front'  => 0,
    'is_configurable'   => 1,
    'used_in_product_listing' => 0,
    'option' => array(
        'values' => array(
            0 => 'Small',
            1 => 'Medium',
            2 => 'Large',
            3 => 'XLarge',
        ),
    ),
));

Mage::getModel('catalog/product')->getResource()->unsetAttributes();

$installer->endSetup();