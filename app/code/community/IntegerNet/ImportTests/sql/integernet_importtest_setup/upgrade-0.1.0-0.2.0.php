<?php
/**
* integer_net Magento Module
*
* @category   IntegerNet
* @package    IntegerNet_ImportTest
* @copyright  Copyright (coffee) 2013 integer_net GmbH (http://www.integer-net.de/)
* @author     Soeren Zorn <sz@integer-net.de>
*/

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->updateAttribute('catalog_product', 'importtest_configurable', 'is_required', 0);

Mage::getModel('catalog/product')->getResource()->unsetAttributes();

$installer->endSetup();