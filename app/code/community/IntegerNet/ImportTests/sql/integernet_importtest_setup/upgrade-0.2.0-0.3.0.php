<?php
/**
* integer_net Magento Module
*
* @category   IntegerNet
* @package    IntegerNet_ImportTest
* @copyright  Copyright (coffee) 2013 integer_net GmbH (http://www.integer-net.de/)
* @author     Soeren Zorn <sz@integer-net.de>
*/

/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$dir = Mage::getBaseDir().DS.'var'.DS.'exim'.DS.'import'.DS;
if(!is_dir($dir))
{
    mkdir($dir);
}

$filename = $dir . 'data.csv';

if (!is_file($filename)) {
    touch($filename);
}

$installer->endSetup();