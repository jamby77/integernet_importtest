<?php

/**
 * @category   IntegerNet
 * @package    IntergerNet_ImportTests
 * @license    http://opensource.org/licenses/osl-3.0.php Open Software Licence 3.0 (OSL-3.0)
 * @author     Soeren Zorn <sz@integer-net.de>
 */

class IntegerNet_ImportTests_Model_Test_DataFlow extends IntegerNet_ImportTests_Model_Test
{
	/**
	 * Run tests
	 *
	 * @return float[]
	 */
	public function runTests()
	{
        $results = array();

        $this->_generate_data();

        if($this->_productType == 'configurable') {
            echo "\nDataFlow cannot yet link simple products to configurable products. But they are inserted anyway.\n";
        }

        $profile = Mage::getModel('dataflow/profile');
        $profile->load('Import All Products Importtest', 'name');
        $profile->delete();

        $data = array(
            'name' => 'Import All Products Importtest',
            'entity_type' => 'product',
            'direction' => 'import',
            'store_id' => 0,
            'gui_data' => array(
                'export' => array(
                    'add_url_field' => 0,
                ),

                'import' => array(
                    'number_of_records' => $this->getChunkSize(),
                    'decimal_separator' => '.',
                ),

                'file' => array(
                    'type' => file,
                    'filename' => 'export_all_products.csv',
                    'path' => 'var/export',
                    'host' => '',
                    'user' => '',
                    'password' => '',
                    'file_mode' => 2,
                    'passive' => '',
                ),

                'parse' => array(
                    'type' => 'csv',
                    'single_sheet' => '',
                    'delimiter' => ',',
                    'enclose' => '"',
                    'fieldnames' => 'true',
                ),

                'map' => array(
                    'only_specified' => '',
                    'product' => array(
                        'db' => array(
                            '0' => 0,
                        ),

                        'file' => array(
                            '0' => '',
                        ),

                    ),

                    'customer' => array(
                        'db' => array(
                            '0' => 0
                        ),

                        'file' => array(
                            '0' => '',
                        ),

                    ),

                ),

                'product' => array(
                    'filter' => array(
                        'name' => '',
                        'sku' => '',
                        'type' => 0,
                        'attribute_set' => '',
                        'price' => array
                        (
                            'from' => '',
                            'to' => '',
                        ),

                        'qty' => array(
                            'from' => '',
                            'to' => '',
                        ),

                        'visibility' => 0,
                        'status' => 0,
                    ),

                ),

                'customer' => array(
                    'filter' => array(
                        'firstname' => '',
                        'lastname' => '',
                        'email' => '',
                        'group' => 0,
                        'adressType' => 'default_billing',
                        'telephone' => '',
                        'postcode' => '',
                        'country' => '',
                        'region' => '',
                        'created_at' => array(
                            'from' => '',
                            'to' => '',
                        ),

                    ),

                ),

            ),

            'data_transfer' => 'interactive',
            'files' => '',
            'actions_xml_view' => '<action type="dataflow/convert_parser_csv" method="parse">
    <var name="delimiter"><![CDATA[,]]></var>
    <var name="enclose"><![CDATA["]]></var>
    <var name="fieldnames">true</var>
    <var name="store"><![CDATA[0]]></var>
    <var name="number_of_records">1</var>
    <var name="decimal_separator"><![CDATA[.]]></var>
    <var name="adapter">catalog/convert_adapter_product</var>
    <var name="method">parse</var>
</action>',
            'page' => 1,
            'limit' => 20,
            'action_code' => '',
            'performed_at' => array(
                'from' => '',
                'to' => '',
                'locale' => 'de_DE',
            ),

            'firstname' => '',
            'lastname' => '',
        );

        $profile = Mage::getModel('dataflow/profile');

        $profile->addData($data);
        $profile->save();

		foreach($this->getRuns() as $run)
		{
			$this->deleteAddedProducts();

			$time = microtime(true);
            $dir = Mage::getBaseDir().DS.'var'.DS.'import'.DS;
            if(!is_dir($dir))
            {
                mkdir($dir);
            }
            copy($this->_cacheFile.$run.'.csv', $dir.'catalog_product.csv');
            Mage::app()->getRequest()->setParam('files', 'catalog_product.csv');
            $profile->run();
            $batchModel = Mage::getSingleton('dataflow/batch');
            if (! $batchModel->getId()) {
                Mage::throwException('Could not load dataflow batch id');
            }
            $batchImportModel = $batchModel->getBatchImportModel();

            $params = $batchModel->getParams();
            $numberOfRecords = $params['number_of_records'];
            $importIds = $batchImportModel->getIdCollection();
            $importCount = count($importIds);

            $chunks = array_chunk($importIds, $numberOfRecords);
            foreach($chunks as $cnt => $chunk)
            {
                $currentImportCount = $cnt*$numberOfRecords+count($chunk);

                $rows = implode(',',$chunk);
                $buffer = shell_exec('php -f '.Mage::getBaseDir().DS.'shell'.DS.'importtest.php -- -dataFlowBatch "'.$batchModel->getId().'" -rows "'.$rows.'"');

                if (empty($buffer))
                {
                    echo 'Response is empty';
                }
                else
                {
                    $result = json_decode($buffer);

                    if (count($result->errors))
                    {
                        foreach ($result->errors as $error)
                        {
                            echo $error."\n";
                        }
                    }
                }
            }

            $buffer = shell_exec('php -f '.Mage::getBaseDir().DS.'shell'.DS.'importtest.php -- -dataFlowFinish "'.$batchModel->getId().'"');

            $result = json_decode($buffer);

            if (count($result->errors))
            {
                foreach ($result->errors as $error)
                {
                    echo $error."\n";
                }
            }

			$results[] = round(microtime(true) - $time, 2);
            Mage::unregister('_singleton/dataflow/batch');
		}
        $profile->delete();
		return $results;
	}

    protected function _generate_data()
    {
        // Create Export file once
        foreach ($this->getRuns() as $test_num)
        {
            if($this->_productType == 'configurable') {
                if(!file_exists($this->_cacheFile.$test_num.'.csv')) {
                    $file = fopen($this->_cacheFile.$test_num.'.csv','w');
                    fwrite($file, '"store","websites","attribute_set","type","category_ids","sku","has_options","name","url_key","url_path","options_container","msrp_enabled","msrp_display_actual_price_type","importtest_configurable","price","weight","status","visibility","enable_googlecheckout","tax_class_id","description","short_description","qty","min_qty","use_config_min_qty","is_qty_decimal","backorders","use_config_backorders","min_sale_qty","use_config_min_sale_qty","max_sale_qty","use_config_max_sale_qty","is_in_stock","low_stock_date","notify_stock_qty","use_config_notify_stock_qty","manage_stock","use_config_manage_stock","stock_status_changed_auto","use_config_qty_increments","qty_increments","use_config_enable_qty_inc","enable_qty_increments","is_decimal_divided","stock_status_changed_automatically","use_config_enable_qty_increments","product_name","store_id","product_type_id","product_status_changed","product_changed_websites"'."\n");
                    for($i = 0; $i < $test_num; $i++)
                    {
                        fwrite($file, '"admin","base","Default","simple","","importtest-configurable-Small-'.$i.'","0","ImportTest Configurable-Small '.$i.'","importtest-configurable-small-'.$i.'","importtest-configurable-small-'.$i.'.html","Block after Info Column","Use config","Use config","Small","20.0000","3.0000","Enabled","Not Visible Individually","Yes","None","Product that can be configured","Product that can be configured","2.0000","0.0000","1","0","0","1","1.0000","1","0.0000","1","1","","1.0000","1","0","1","0","1","0.0000","1","0","0","0","1","ImportTest Configurable-Small 0","0","simple","",""'."\n");
                        fwrite($file, '"admin","base","Default","simple","","importtest-configurable-Medium-'.$i.'","0","ImportTest Configurable-Medium '.$i.'","importtest-configurable-medium-'.$i.'","importtest-configurable-medium-'.$i.'.html","Block after Info Column","Use config","Use config","Medium","20.0000","3.0000","Enabled","Not Visible Individually","Yes","None","Product that can be configured","Product that can be configured","2.0000","0.0000","1","0","0","1","1.0000","1","0.0000","1","1","","1.0000","1","0","1","0","1","0.0000","1","0","0","0","1","ImportTest Configurable-Medium 0","0","simple","",""'."\n");
                        fwrite($file, '"admin","base","Default","simple","","importtest-configurable-Large-'.$i.'","0","ImportTest Configurable-Large '.$i.'","importtest-configurable-large-'.$i.'","importtest-configurable-large-'.$i.'.html","Block after Info Column","Use config","Use config","Large","20.0000","3.0000","Enabled","Not Visible Individually","Yes","None","Product that can be configured","Product that can be configured","2.0000","0.0000","1","0","0","1","1.0000","1","0.0000","1","1","","1.0000","1","0","1","0","1","0.0000","1","0","0","0","1","ImportTest Configurable-Large 0","0","simple","",""'."\n");
                        fwrite($file, '"admin","base","Default","simple","","importtest-configurable-XLarge-'.$i.'","0","ImportTest Configurable-XLarge '.$i.'","importtest-configurable-xlarge-'.$i.'","importtest-configurable-xlarge-'.$i.'.html","Block after Info Column","Use config","Use config","XLarge","20.0000","3.0000","Enabled","Not Visible Individually","Yes","None","Product that can be configured","Product that can be configured","2.0000","0.0000","1","0","0","1","1.0000","1","0.0000","1","1","","1.0000","1","0","1","0","1","0.0000","1","0","0","0","1","ImportTest Configurable-XLarge 0","0","simple","",""'."\n");
                        fwrite($file, '"admin","base","Default","configurable","","importtest-configurable-'.$i.'","0","ImportTest Configurable '.$i.'","importtest-configurable-'.$i.'","importtest-configurable-'.$i.'.html","Block after Info Column","Use config","Use config","","20.0000","","Enabled","Catalog, Search","Yes","None","Product that can be configured","Product that can be configured","0.0000","0.0000","1","0","0","1","1.0000","1","0.0000","1","1","","1.0000","1","0","1","0","1","0.0000","1","0","0","0","1","ImportTest Configurable 0","0","configurable","",""'."\n");
                    }
                }
            }
            else {
                if(!file_exists($this->_cacheFile.$test_num.'.csv'))
                {
                    $file = fopen($this->_cacheFile.$test_num.'.csv','w');
                    fwrite($file, '"store","websites","attribute_set","type","category_ids","sku","has_options","name","url_key","url_path","options_container","msrp_enabled","msrp_display_actual_price_type","price","weight","status","visibility","enable_googlecheckout","tax_class_id","description","short_description","qty","min_qty","use_config_min_qty","is_qty_decimal","backorders","use_config_backorders","min_sale_qty","use_config_min_sale_qty","max_sale_qty","use_config_max_sale_qty","is_in_stock","low_stock_date","notify_stock_qty","use_config_notify_stock_qty","manage_stock","use_config_manage_stock","stock_status_changed_auto","use_config_qty_increments","qty_increments","use_config_enable_qty_inc","enable_qty_increments","is_decimal_divided","stock_status_changed_automatically","use_config_enable_qty_increments","product_name","store_id","product_type_id","product_status_changed","product_changed_websites"'."\n");
                    for($i = 0; $i < $test_num; $i++)
                    {
                        fwrite($file, '"admin","base","Default","simple","","importtest'.$i.'","'.$i.'","Test '.$i.'","test-'.$i.'","test-'.$i.'.html","Block after Info Column","Use config","Use config","20.0000","4.0000","Enabled","Catalog, Search","Yes","None","Das ist ein Test 0","Testprodukt 0","20.0000","0.0000","1","0","0","1","0.0000","0","1000.0000","0","1","","","1","1","0","0","1","0.0000","1","0","0","0","1","Test 0","0","simple","",""'."\n");
                    }
                    fclose($file);
                }
            }
        }
    }

    public function runBatch($batchId, $rowIds)
    {
        /* @var $batchModel Mage_Dataflow_Model_Batch */
        $batchModel = Mage::getModel('dataflow/batch')->load($batchId);

        if (!$batchModel->getId()) {
            return;
        }
        if (!is_array($rowIds) || count($rowIds) < 1) {
            return;
        }
        if (!$batchModel->getAdapter()) {
            return;
        }

        $batchImportModel = $batchModel->getBatchImportModel();
        $importIds = $batchImportModel->getIdCollection();

        $adapter = Mage::getModel($batchModel->getAdapter());
        $adapter->setBatchParams($batchModel->getParams());

        $errors = array();
        $saved  = 0;
        foreach ($rowIds as $importId) {
            $batchImportModel->load($importId);
            if (!$batchImportModel->getId()) {
                $errors[] = Mage::helper('dataflow')->__('Skip undefined row.');
                continue;
            }

            try {
                $importData = $batchImportModel->getBatchData();
                $adapter->saveRow($importData);
            } catch (Exception $e) {
                $errors[] = $e->getMessage();
                continue;
            }
            $saved ++;
        }

        if (method_exists($adapter, 'getEventPrefix')) {
            /**
             * Event for process rules relations after products import
             */
            Mage::dispatchEvent($adapter->getEventPrefix() . '_finish_before', array(
                'adapter' => $adapter
            ));

            /**
             * Clear affected ids for adapter possible reuse
             */
            $adapter->clearAffectedEntityIds();
        }

        $result = array(
            'savedRows' => $saved,
            'errors'    => $errors
        );
        echo Mage::helper('core')->jsonEncode($result);
    }

    public function batchFinish($batchId)
    {
        if ($batchId) {
            $batchModel = Mage::getModel('dataflow/batch')->load($batchId);
            /* @var $batchModel Mage_Dataflow_Model_Batch */

            if ($batchModel->getId()) {
                $result = array();
                try {
                    $batchModel->beforeFinish();
                } catch (Mage_Core_Exception $e) {
                    $result['error'] = $e->getMessage();
                } catch (Exception $e) {
                    $result['error'] = Mage::helper('adminhtml')->__('An error occurred while finishing process. Please refresh the cache');
                }
                $batchModel->delete();
                echo Mage::helper('core')->jsonEncode($result);
            }
        }
    }

}
