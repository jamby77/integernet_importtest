<?php

/**
 * @category   IntegerNet
 * @package    IntergerNet_ImportTests
 * @license    http://opensource.org/licenses/osl-3.0.php Open Software Licence 3.0 (OSL-3.0)
 * @author     Soeren Zorn <sz@integer-net.de>
 */

class IntegerNet_ImportTests_Model_Test_Mage extends IntegerNet_ImportTests_Model_Test
{
    /**
     * Run tests
     *
     * @return float[]
     */
    public function runTests()
    {
        $results = array();

        $importData = $this->_generate_data();

        foreach ($this->getRuns() as $run) {
            $this->deleteAddedProducts();

            $time = microtime(true);

            for ($i = 0; $i < $run; $i++) {
                if ($this->_productType == 'configurable') {
                    $productIds = array();
                    foreach ($importData[$run][$i]['simple'] as $productData) {

                        $product = Mage::getModel('catalog/product');

                        $product->setData($productData);

                        $product->save();

                        $productIds[] = $product->getId();
                    }
                    $product = Mage::getModel('catalog/product');

                    $product->setData($importData[$run][$i]['configurable']);

                    $attributeId = Mage::getSingleton('eav/entity_attribute')->getIdByCode(Mage_Catalog_Model_Product::ENTITY, 'importtest_configurable');
                    $product->getTypeInstance()->setUsedProductAttributeIds(array($attributeId));

                    $attribute = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);

                    $data = array(
                        $productIds[0] => array(
                            '0' => array(
                                'attribute_id' => $attributeId,
                                'label' => 'Small',
                                'value_index' => $attribute->getSource()->getOptionId('Small'),
                                'is_percent' => 0,
                                'pricing_value' => ''
                            ),
                        ),
                        $productIds[1] => array(
                            '0' => array(
                                'attribute_id' => $attributeId,
                                'label' => 'Medium',
                                'value_index' => $attribute->getSource()->getOptionId('Medium'),
                                'is_percent' => 0,
                                'pricing_value' => ''
                            ),
                        ),
                        $productIds[2] => array(
                            '0' => array(
                                'attribute_id' => $attributeId,
                                'label' => 'Large',
                                'value_index' => $attribute->getSource()->getOptionId('Large'),
                                'is_percent' => 0,
                                'pricing_value' => ''
                            ),
                        ),
                        $productIds[3] => array(
                            '0' => array(
                                'attribute_id' => $attributeId,
                                'label' => 'XLarge',
                                'value_index' => $attribute->getSource()->getOptionId('XLarge'),
                                'is_percent' => 0,
                                'pricing_value' => ''
                            ),
                        ),
                    );
                    $product->setConfigurableProductsData($data);

                    $data = array(
                        '0' => array(
                            'label' => 'ImportTest-Configure-Input',
                            'position' => NULL,
                            'values' => array(
                                '0' => array(
                                    'value_index' => $attribute->getSource()->getOptionId('Small'),
                                    'label' => 'Small',
                                    'is_percent' => 0,
                                    'pricing_value' => '0',
                                    'attribute_id' => $attributeId,
                                ),
                                '1' => array(
                                    'value_index' => $attribute->getSource()->getOptionId('Medium'),
                                    'label' => 'Medium',
                                    'is_percent' => 0,
                                    'pricing_value' => '0',
                                    'attribute_id' => $attributeId,
                                ),
                                '2' => array(
                                    'value_index' => $attribute->getSource()->getOptionId('Large'),
                                    'label' => 'Large',
                                    'is_percent' => 0,
                                    'pricing_value' => '0',
                                    'attribute_id' => $attributeId,
                                ),
                                '3' => array(
                                    'value_index' => $attribute->getSource()->getOptionId('XLarge'),
                                    'label' => 'XLarge',
                                    'is_percent' => 0,
                                    'pricing_value' => '0',
                                    'attribute_id' => $attributeId,
                                ),
                            ),
                            'attribute_id' => $attributeId,
                            'attribute_code' => 'importtest_configurable',
                            'frontend_label' => 'ImportTest-Configure-Input',
                            'html_id' => 'config_super_product__attribute_0'
                        ),
                    );
                    $product->setConfigurableAttributesData($data);

                    $product->setCanSaveConfigurableAttributes(true);

                    $product->save();

                } else {
                    $product = Mage::getModel('catalog/product');

                    $product->setData($importData[$run][$i]);

                    $product->save();
                }
            }

            $results[] = round(microtime(true) - $time, 2);
        }
        return $results;
    }

    protected function _generate_data()
    {
        $dataSkeleton = array(
            'attribute_set_id' => 4,
            'type_id' => 'simple',
            'weight' => 4,
            'status' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED,
            'visibility' => Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE,
            'tax_class_id' => 0,
            'price' => 20,
            'stock_data' => array(
                'qty' => 20,
                'is_in_stock' => 1,
                'stock_id' => 1,
                'store_id' => 1,
                'manage_stock' => 1,
                'use_config_manage_stock' => 1,
                'use_config_min_sale_qty' => 1,
                'use_config_max_sale_qty' => 1,
            ),
        );

        $attributeId = Mage::getSingleton('eav/entity_attribute')->getIdByCode(Mage_Catalog_Model_Product::ENTITY, 'importtest_configurable');

        $attribute = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);

        $importData = array();
        foreach ($this->getRuns() as $run) {
            $importData[$run] = array();

            for ($i = 0; $i < $run; $i++) {
                if ($this->_productType == 'configurable') {
                    $importData[$run][$i] = array();
                    $importData[$run][$i]['simple'] = array();
                    $importData[$run][$i]['simple'][] = array_merge($dataSkeleton, array(
                        'sku' => 'importtest-configurable-Small-' . $i,
                        'name' => 'ImportTest Configurable-Small ' . $i,
                        'description' => 'Das ist ein Test ' . $i,
                        'short_description' => 'Testprodukt ' . $i,
                        'importtest_configurable' => $attribute->getSource()->getOptionId('Small'),
                    ));
                    $importData[$run][$i]['simple'][] = array_merge($dataSkeleton, array(
                        'sku' => 'importtest-configurable-Medium-' . $i,
                        'name' => 'ImportTest Configurable-Medium ' . $i,
                        'description' => 'Das ist ein Test ' . $i,
                        'short_description' => 'Testprodukt ' . $i,
                        'importtest_configurable' => $attribute->getSource()->getOptionId('Medium'),
                    ));
                    $importData[$run][$i]['simple'][] = array_merge($dataSkeleton, array(
                        'sku' => 'importtest-configurable-Large-' . $i,
                        'name' => 'ImportTest Configurable-Large ' . $i,
                        'description' => 'Das ist ein Test ' . $i,
                        'short_description' => 'Testprodukt ' . $i,
                        'importtest_configurable' => $attribute->getSource()->getOptionId('Large'),
                    ));
                    $importData[$run][$i]['simple'][] = array_merge($dataSkeleton, array(
                        'sku' => 'importtest-configurable-XLarge-' . $i,
                        'name' => 'ImportTest Configurable-XLarge ' . $i,
                        'description' => 'Das ist ein Test ' . $i,
                        'short_description' => 'Testprodukt ' . $i,
                        'importtest_configurable' => $attribute->getSource()->getOptionId('XLarge'),
                    ));
                    $importData[$run][$i]['configurable'] = array_merge($dataSkeleton, array(
                        'type_id' => 'configurable',
                        'sku' => 'importtest-configurable-' . $i,
                        'name' => 'ImportTest Configurable ' . $i,
                        'description' => 'Das ist ein Test ' . $i,
                        'short_description' => 'Testprodukt ' . $i,
                        'visibility' => Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
                        'has_options' => 1,
                    ));
                } else {
                    $importData[$run][$i] = array(
                        'attribute_set_id' => 4,
                        'type_id' => 'simple',
                        'sku' => 'importtest' . $i,
                        'name' => 'Test ' . $i,
                        'description' => 'Das ist ein Test ' . $i,
                        'short_description' => 'Testprodukt ' . $i,
                        'weight' => 4,
                        'status' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED,
                        'visibility' => Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
                        'tax_class_id' => 0,
                        'price' => 20,
                        'stock_data' => array(
                            'qty' => 20,
                            'is_in_stock' => 1,
                            'stock_id' => 1,
                            'store_id' => 1,
                            'manage_stock' => 1,
                            'use_config_manage_stock' => 1,
                            'use_config_min_sale_qty' => 1,
                            'use_config_max_sale_qty' => 1,
                        ),
                    );
                }
            }
        }
        return $importData;
    }

}
