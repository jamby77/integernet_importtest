<?php

/**
 * @category   IntegerNet
 * @package    IntergerNet_ImportTests
 * @license    http://opensource.org/licenses/osl-3.0.php Open Software Licence 3.0 (OSL-3.0)
 * @author     Soeren Zorn <sz@integer-net.de>
 */

class IntegerNet_ImportTests_Model_Test_Soap extends IntegerNet_ImportTests_Model_Test
{
    public function checkPrerequisites()
    {
        $options = array(
            'trace' => true,
            'connection_timeout' => 120,
            //'wsdl_cache' => WSDL_CACHE_NONE,
        );
        $client = new SoapClient(Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . 'api/v2_soap/?wsdl=1', $options);
        // If some stuff requires api authentification,
        // then get a session token
        $apiUser = Mage::getStoreConfig('ImportTest/ApiUser');
        $apiKey = Mage::getStoreConfig('ImportTest/ApiKey');

        try
        {
            $client->login($apiUser, $apiKey);
        }
        catch(Exception $e)
        {
            Mage::throwException("Soap ApiUser and/or ApiKey are not valid. Please specify ApiKey and ApiUser first: \n php importtest.php -ApiUser <ApiUser>\n php importtest.php -ApiKey <ApiKey>\n ApiUser must be equal to admin user \n WSDL cache should be disabled");
        }
    }

	/**
	 * Run tests
	 *
	 * @return float[]
	 */
	public function runTests()
	{
        $results = array();

        $importData = $this->_generate_data();

		foreach($this->getRuns() as $run)
		{
			$this->deleteAddedProducts();

			$time = microtime(true);

            $options = array(
                'trace' => true,
                'connection_timeout' => 120,
                //'wsdl_cache' => WSDL_CACHE_NONE,
            );
            $client = new SoapClient(Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . 'api/v2_soap/?wsdl=1', $options);

            // If some stuff requires api authentification,
            // then get a session token
            $apiUser = Mage::getStoreConfig('ImportTest/ApiUser');
            $apiKey = Mage::getStoreConfig('ImportTest/ApiKey');
            try
            {
                $session = $client->login($apiUser, $apiKey);
            }
            catch(Exception $e)
            {
                echo "\nSoap ApiUser and/or ApiKey are not valid. Please specify ApiKey and ApiUser first:\n";
                echo "php importtest.php -ApiUser <ApiUser>\n";
                echo "php importtest.php -ApiKey <ApiKey>\n";
                exit;
            }

			foreach($importData[$run] as $productData)
			{
                try{
                    $productSku = $productData['sku'];
                    $attributeSet = $productData['attribute_set'];
                    $productType = $productData['type'];
                    unset($productData['sku']);
                    unset($productData['attribute_set']);
                    unset($productData['type']);
                    $client->catalogProductCreate($session, $productType, $attributeSet, $productSku, $productData);
                }
                catch(Exception $e)
                {
                    print_r($e);
                    echo "\nSoap-User does not have permissions to add products. Go to Magento backend and change roles in System/Web Services/SOAP / XML-RPC Roles\n";
                    exit;
                }
			}

			$results[] = round(microtime(true) - $time, 2);
		}
		return $results;
	}

    protected function _generate_data()
    {
        $importData = array();
        $stockData = array(
            'qty' => 20,
            'is_in_stock' => 1,
            'stock_id' => 1,
            'store_id' => 1,
            'manage_stock' => 1,
            'use_config_manage_stock' => 1,
            'use_config_min_sale_qty' => 1,
            'use_config_max_sale_qty' => 1,
        );
        $arraySkeleton = array(
            'website_ids' => array('base'),
            'store' => 'default',
            'attribute_set' => 4,
            'type' => 'simple',
            'weight' => 4,
            'status' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED,
            'visibility' => Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE,
            'tax_class_id' => 'None',
            'price' => 20,
            'stock_data' => $stockData,
        );
        foreach($this->getRuns() as $run) {

            $importData[$run] = array();
            for ($i = 0; $i < $run; $i++) {
                if($this->_productType == 'configurable') {

                    $importData[$run][] = array_merge($arraySkeleton, array(
                        'sku' => 'importtest-configurable-Small-'.$i,
                        'name' => 'ImportTest Configurable-Small ' . $i,
                        'description' => 'Das ist ein Test ' . $i,
                        'short_description' => 'Testprodukt ' . $i,
                        'additional_attributes' => array(
                            'single_data' => array(
                                array(
                                    'key'   => 'importtest_configurable',
                                    'value' => 'Small',
                                ),
                            ),
                        ),
                    ));

                    $importData[$run][] = array_merge($arraySkeleton, array(
                        'sku' => 'importtest-configurable-Medium-'.$i,
                        'name' => 'ImportTest Configurable-Medium ' . $i,
                        'description' => 'Das ist ein Test ' . $i,
                        'short_description' => 'Testprodukt ' . $i,
                        'additional_attributes' => array(
                            'single_data' => array(
                                array(
                                    'key'   => 'importtest_configurable',
                                    'value' => 'Medium',
                                ),
                            ),
                        ),
                    ));

                    $importData[$run][] = array_merge($arraySkeleton, array(
                        'sku' => 'importtest-configurable-Large-'.$i,
                        'name' => 'ImportTest Configurable-Large ' . $i,
                        'description' => 'Das ist ein Test ' . $i,
                        'short_description' => 'Testprodukt ' . $i,
                        'additional_attributes' => array(
                            'single_data' => array(
                                array(
                                    'key'   => 'importtest_configurable',
                                    'value' => 'Large',
                                ),
                            ),
                        ),
                    ));

                    $importData[$run][] = array_merge($arraySkeleton, array(
                        'sku' => 'importtest-configurable-XLarge-'.$i,
                        'name' => 'ImportTest Configurable-XLarge ' . $i,
                        'description' => 'Das ist ein Test ' . $i,
                        'short_description' => 'Testprodukt ' . $i,
                        'additional_attributes' => array(
                            'single_data' => array(
                                array(
                                    'key'   => 'importtest_configurable',
                                    'value' => 'XLarge',
                                ),
                            ),
                        ),
                    ));

                    $importData[$run][] = array_merge($arraySkeleton, array(
                        'type' => 'configurable',
                        'sku' => 'importtest-configurable-'.$i,
                        'name' => 'ImportTest Configurable ' . $i,
                        'description' => 'Das ist ein Test ' . $i,
                        'short_description' => 'Testprodukt ' . $i,
                        'visibility' => Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
                        'has_options' => 1,
                        'associated_skus' => array('importtest-configurable-Small-'.$i, 'importtest-configurable-Medium-'.$i, 'importtest-configurable-Large-'.$i, 'importtest-configurable-XLarge-'.$i),
                        'configurable_attributes' => 'importtest_configurable',
                    ));
                }
                else {
                    $importData[$run][$i] = array(
                        'sku' => 'importtest-'.$i,
                        'attribute_set' => 4,
                        'type' => 'simple',
                        'name' => 'Test ' . $i,
                        'website_ids' => array('base'),
                        'description' => 'Das ist ein Test ' . $i,
                        'short_description' => 'Testprodukt ' . $i,
                        'weight' => 4,
                        'status' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED,
                        'visibility' => Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
                        'tax_class_id' => 0,
                        'price' => 20,
                        'stock_data' => $stockData,
                    );
                }
            }
        }
        return $importData;
    }
}