<?php

/**
 * @category   IntegerNet
 * @package    IntergerNet_ImportTests
 * @license    http://opensource.org/licenses/osl-3.0.php Open Software Licence 3.0 (OSL-3.0)
 * @author     Soeren Zorn <sz@integer-net.de>
 */

class IntegerNet_ImportTests_Model_Test_Rest extends IntegerNet_ImportTests_Model_Test
{
    public function checkPrerequisites()
    {
        if (!class_exists('OAuth')) {
            Mage::throwException('Please install the OAuth extension for PHP.');
        }
    }

	/**
	 * Run tests
	 *
	 * @return float[]
	 */
	public function runTests()
	{
        if($this->_productType == 'configurable') {
            echo "\nRest does not support configurable products. Adding simple products...\n";
        }

        $baseUri = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
		$apiUrl = $baseUri.'api/rest';
		$consumerKey = Mage::getStoreConfig('ImportTest/ConsumerKey');
		$consumerSecret = Mage::getStoreConfig('ImportTest/ConsumerSecret');
        $oauthToken = Mage::getStoreConfig('ImportTest/OauthToken');
        $oauthTokenSecret = Mage::getStoreConfig('ImportTest/OauthTokenSecret');

        $results = array();

        $importData = $this->_generate_data();
        
        try {
            $oauthClient = new OAuth($consumerKey, $consumerSecret, OAUTH_SIG_METHOD_HMACSHA1, OAUTH_AUTH_TYPE_AUTHORIZATION);
            $oauthClient->enableDebug();

            $oauthClient->setToken($oauthToken, $oauthTokenSecret);
            $headers = array('Content-Type' => 'application/json');
            $resourceUrl = "$apiUrl/products";

            foreach($this->getRuns() as $run)
            {
                $this->deleteAddedProducts();

                $time = microtime(true);
                for ($i = 0; $i < $run; $i++)
                {
                    $oauthClient->fetch($resourceUrl, json_encode($importData[$run][$i]), OAUTH_HTTP_METHOD_POST, $headers);
                }
                $results[] = round(microtime(true) - $time, 2);
            }
        } catch (OAuthException $e) {
            $url = Mage::getStoreConfig('web/unsecure/base_url');
            echo "\nThere is an error using Rest. Please visit {$url}getaccesstoken first.\n";
            exit;
        } catch (Exception $e) {
            echo "\n" . $e->getMessage() . "\n";
        }
		return $results;
	}

    protected function _generate_data()
    {
        $importData = array();
        foreach($this->getRuns() as $run)
        {
            $importData[$run] = array();
            for ($i = 0; $i < $run; $i++)
            {
                $stock_data = array(
                    'qty' => 20,
                    'is_in_stock' => 1,
                    'stock_id' => 1,
                    'store_id' => 1,
                    'manage_stock' => 1,
                    'use_config_manage_stock' => 0,
                    'min_sale_qty' => 0,
                    'use_config_min_sale_qty' => 0,
                    'max_sale_qty' => 1000,
                    'use_config_max_sale_qty' => 0,
                );
                $importData[$run][$i] = array(
                    'attribute_set_id' => 4,
                    'type_id' => 'simple',
                    'sku' => 'importtest' . $i,
                    'name' => 'Test ' . $i,
                    'description' => 'Das ist ein Test ' . $i,
                    'short_description' => 'Testprodukt ' . $i,
                    'weight' => 4,
                    'status' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED,
                    'visibility' => Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
                    'tax_class_id' => 0,
                    'price' => 20,
                    'stock_data' => $stock_data,
                );
            }
        }
        return $importData;
    }

}
