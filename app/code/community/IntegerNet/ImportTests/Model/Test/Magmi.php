<?php

/**
 * @category   IntegerNet
 * @package    IntergerNet_ImportTests
 * @license    http://opensource.org/licenses/osl-3.0.php Open Software Licence 3.0 (OSL-3.0)
 * @author     Soeren Zorn <sz@integer-net.de>
 */

class IntegerNet_ImportTests_Model_Test_Magmi extends IntegerNet_ImportTests_Model_Test
{
	/**
	 * Run tests
	 *
	 * @return float[]
	 */
	public function runTests()
	{
        $results = array();
        
        require_once(Mage::getBaseDir().DS.'magmi'.DS.'inc'.DS.'magmi_defs.php');
        require_once(Mage::getBaseDir().DS.'magmi'.DS.'integration'.DS.'inc'.DS.'magmi_datapump.php');

        if($this->_productType == 'configurable') {
            echo "\nIf configurable Products are not linked, please visit ".Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB)."magmi/web/magmi.php and check Configurable Item processor and save Profile.\n";
        }

        $importData = $this->_generate_data();

		foreach($this->getRuns() as $run)
		{
			$this->deleteAddedProducts();

			$time = microtime(true);
			// create a Product import Datapump using Magmi_DatapumpFactory

			$dp=Magmi_DataPumpFactory::getDataPumpInstance("productimport");

            try
            {
			    $dp->beginImportSession("default","create");
            }
            catch(Exception $e)
            {
                echo "\nMagmi database login data is wrong. Please update magmi/conf/magmi.ini\n";
                exit;
            }

            foreach($this->getRuns() as $run) {
                foreach($importData[$run] as $productData) {
                    // Now ingest item into magento
                    $dp->ingest($productData);
                }
			}
			// End import Session
			$dp->endImportSession();
			$results[] = round(microtime(true) - $time, 2);
		}
		return $results;
	}

    protected function _generate_data()
    {
        $arraySkeleton = array(
            'store' => 'default',
            'attribute_set' => 'Default',
            'type' => 'simple',
            'weight' => 4,
            'status' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED,
            'visibility' => Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE,
            'tax_class_id' => 'None',
            'price' => 20,

            'qty' => 20,
            'is_in_stock' => 1,
            'stock_id' => 1,
            'store_id' => 1,
            'manage_stock' => 1,
            'use_config_manage_stock' => 1,
            'use_config_min_sale_qty' => 1,
            'use_config_max_sale_qty' => 1,
        );

        $attributeId = Mage::getSingleton('eav/entity_attribute')->getIdByCode(Mage_Catalog_Model_Product::ENTITY, 'importtest_configurable');

        $attribute = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);

        $importData = array();
        foreach($this->getRuns() as $run) {
            $importData[$run] = array();
            for ($i = 0; $i < $run; $i++) {
                if($this->_productType == 'configurable') {
                    $importData[$run][] = array_merge($arraySkeleton, array(
                        'sku' => 'importtest-configurable-Small-'.$i,
                        'name' => 'ImportTest Configurable-Small ' . $i,
                        'description' => 'Das ist ein Test ' . $i,
                        'short_description' => 'Testprodukt ' . $i,
                        'importtest_configurable' => $attribute->getSource()->getOptionId('Small'),
                    ));

                    $importData[$run][] = array_merge($arraySkeleton, array(
                        'sku' => 'importtest-configurable-Medium-'.$i,
                        'name' => 'ImportTest Configurable-Medium ' . $i,
                        'description' => 'Das ist ein Test ' . $i,
                        'short_description' => 'Testprodukt ' . $i,
                        'importtest_configurable' => $attribute->getSource()->getOptionId('Medium'),
                    ));

                    $importData[$run][] = array_merge($arraySkeleton, array(
                        'sku' => 'importtest-configurable-Large-'.$i,
                        'name' => 'ImportTest Configurable-Large ' . $i,
                        'description' => 'Das ist ein Test ' . $i,
                        'short_description' => 'Testprodukt ' . $i,
                        'importtest_configurable' => $attribute->getSource()->getOptionId('Large'),
                    ));

                    $importData[$run][] = array_merge($arraySkeleton, array(
                        'sku' => 'importtest-configurable-XLarge-'.$i,
                        'name' => 'ImportTest Configurable-XLarge ' . $i,
                        'description' => 'Das ist ein Test ' . $i,
                        'short_description' => 'Testprodukt ' . $i,
                        'importtest_configurable' => $attribute->getSource()->getOptionId('XLarge'),
                    ));

                    $importData[$run][] = array_merge($arraySkeleton, array(
                        'type' => 'configurable',
                        'sku' => 'importtest-configurable-'.$i,
                        'name' => 'ImportTest Configurable ' . $i,
                        'description' => 'Das ist ein Test ' . $i,
                        'short_description' => 'Testprodukt ' . $i,
                        'visibility' => Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
                        'has_options' => 1,
                        'simples_skus' => 'importtest-configurable-Small-'.$i.',importtest-configurable-Medium-'.$i.',importtest-configurable-Large-'.$i.',importtest-configurable-XLarge-'.$i,
                        'configurable_attributes' => 'importtest_configurable',
                    ));
                }
                else {
                    $importData[$run][$i] = array(
                        'store' => 'default',
                        'attribute_set' => 'Default',
                        'type' => 'simple',
                        'sku' => 'importtest' . $i,
                        'name' => 'Test ' . $i,
                        'description' => 'Das ist ein Test ' . $i,
                        'short_description' => 'Testprodukt ' . $i,
                        'weight' => 4,
                        'status' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED,
                        'visibility' => Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
                        'tax_class_id' => 'None',
                        'price' => 20,

                        'qty' => 20,
                        'is_in_stock' => 1,
                        'stock_id' => 1,
                        'store_id' => 1,
                        'manage_stock' => 1,
                        'use_config_manage_stock' => 1,
                        'use_config_min_sale_qty' => 1,
                        'use_config_max_sale_qty' => 1,
                    );
                }
            }
        }
        return $importData;
    }

}
