<?php

/**
 * @category   IntegerNet
 * @package    IntergerNet_ImportTests
 * @license    http://opensource.org/licenses/osl-3.0.php Open Software Licence 3.0 (OSL-3.0)
 * @author     Soeren Zorn <sz@integer-net.de>
 */

class IntegerNet_ImportTests_Model_Test_URapidFlow extends IntegerNet_ImportTests_Model_Test
{

    public function checkPrerequisites()
    {
        if (!extension_loaded('ionCube Loader')) {
            $__oc = strtolower(substr(php_uname(), 0, 3));
            $__ln = 'ioncube_loader_' . $__oc . '_' . substr(phpversion(), 0, 3) . (($__oc == 'win') ? '.dll' : '.so');
            @dl($__ln);
            if (function_exists('_il_exec')) {
                return _il_exec();
            }
            $__ln = '/ioncube/' . $__ln;
            $__oid = $__id = realpath(ini_get('extension_dir'));
            $__here = dirname(__FILE__);
            if (strlen($__id) > 1 && $__id[1] == ':') {
                $__id = str_replace('\\', '/', substr($__id, 2));
                $__here = str_replace('\\', '/', substr($__here, 2));
            }
            $__rd = str_repeat('/..', substr_count($__id, '/')) . $__here . '/';
            $__i = strlen($__rd);
            while ($__i--) {
                if ($__rd[$__i] == '/') {
                    $__lp = substr($__rd, 0, $__i) . $__ln;
                    if (file_exists($__oid . $__lp)) {
                        $__ln = $__lp;
                        break;
                    }
                }
            }
            @dl($__ln);
            if (!function_exists('_il_exec')) {
                Mage::throwException('Site error: the file <b>' . __FILE__ . '</b> requires the ionCube PHP Loader ' . basename($__ln) . ' to be installed by the site administrator.');
            }
        }
    }
	/**
	 * Run tests
	 *
	 * @return float[]
	 */
	public function runTests()
	{
        $results = array();

        $this->_generate_data();

        $data = array(
            'title' => 'Test',
            'profile_status' => 'enabled',
            'profile_type' => 'import',
            'data_type' => 'product',
            'store_id' => 0,
            'base_dir' => '',
            'filename' => 'catalog_product.csv',
            'options' => array (
                'log' => array (
                    'min_level' => 'SUCCESS'
                ),
                'remote' => array(
                    'type' => '',
                    'host' => array(
                        'port' => '',
                        'username' => '',
                        'password' => '',
                        'path' => '',
                    ),
                ),
                'encoding' => array(
                    'from'  => '',
                    'illegal_char' => '',
                ),
                'csv' => array(
                    'delimiter' => ',',
                    'enclosure' => '"',
                    'multivalue_separator' => ';',
                ),
                'import' => Array
                (
                    'actions' => 'any',
                    'dryrun' => 0,
                    'change_typeset' => 0,
                    'select_ids' => 0,
                    'not_applicable' => 0,
                    'store_value_same_as_default' => 'default',
                    'stock_zero_out' => 0,
                    'reindex_type' => 'realtime',
                    'image_files' => 0,
                    'image_files_remote' => 0,
                    'image_remote_subfolder_level' => '',
                    'image_missing_file' => 'warning_save',
                    'create_options' => 0,
                    'create_categories' => 0,
                    'create_categories_active' => 0,
                    'create_categories_anchor' => 0,
                    'create_categories_display' => 'PRODUCTS',
                    'create_categories_menu' => 0,
                    'delete_old_category_products' => 0,
                    'create_attributesets' => 0,
                    'create_attributeset_template' => 4,
                    'insert_attr_chunk_size' => '',
                ),
                'dir' => array(
                    'images' => '',
                ),
            ),
            'json_import' => '',
            'columns_post' => array(),
        );

        $model = Mage::getModel('urapidflow/profile');
        $model->addData($data);
        $model = $model->factory();
        if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
            $model->setCreatedTime(now())
                ->setUpdateTime(now());
        } else {
            $model->setUpdateTime(now());
        }
        $model->save();
        $model->pending('ondemand')->save();

		foreach($this->getRuns() as $run)
		{
			$this->deleteAddedProducts();

			$time = microtime(true);
            $dir = Mage::getBaseDir().DS.'var'.DS.'urapidflow'.DS.'import'.DS;
            if(!is_dir($dir))
            {
                mkdir($dir);
            }
            copy($this->_cacheFile.$run.'.csv', $dir .'catalog_product.csv');
            $model->start()->save()->run();

			$results[] = round(microtime(true) - $time, 2);
		}
        $model->delete();
		return $results;
	}

    protected function _generate_data()
    {
        // Create Export file once
        foreach ($this->getRuns() as $test_num)
        {
            if($this->_productType == 'configurable') {
                if(!file_exists($this->_cacheFile.$test_num.'.csv')) {
                    $file = fopen($this->_cacheFile.$test_num.'.csv','w');
                    fwrite($file, 'color,cost,country_of_manufacture,created_at,custom_design,custom_design_from,custom_design_to,custom_layout_update,description,enable_googlecheckout,gift_message_available,group_price,image,image_label,importtest_configurable,is_recurring,manufacturer,meta_description,meta_keyword,meta_title,msrp,msrp_display_actual_price_type,msrp_enabled,name,news_from_date,news_to_date,old_id,options_container,page_layout,price,price_view,recurring_profile,short_description,size,sku,small_image,small_image_label,special_from_date,special_price,special_to_date,status,tax_class_id,thumbnail,thumbnail_label,tier_price,updated_at,url_key,url_path,visibility,weight,product.attribute_set,product.type,product.websites,category.ids,category.path,category.name,product.has_options,product.required_options,stock.is_in_stock,stock.backorders,stock.manage_stock,stock.use_config_manage_stock,stock.is_qty_decimal,stock.use_config_notify_stock_qty,stock.use_config_min_qty,stock.use_config_backorders,stock.use_config_min_sale_qty,stock.use_config_max_sale_qty,stock.stock_status_changed_automatically,stock.use_config_enable_qty_increments,stock.enable_qty_increments,stock.use_config_qty_increments,stock.qty,stock.min_qty,stock.min_sale_qty,stock.max_sale_qty,stock.notify_stock_qty,stock.qty_increments,price_type,weight_type,sku_type,shipment_type,price.final,price.minimal,price.maximum'."\n");
                    for($i = 0; $i < $test_num; $i++) {
                        fwrite($file, ',,,"2013-10-01 14:58:53",,,,,"Das ist ein Test '.$i.'",Yes,,,,,,,,,,,,,"Test '.$i.'",,,,"Block after Info Column",,20.0000,,,"Testprodukt '.$i.'",importtest'.$i.',,,,,,Enabled,None,,,,"2013-10-02 10:07:16",test-'.$i.',test-'.$i.'.html,"Catalog, Search",4.0000,Default,simple,base,,,,No,No,"In Stock","No Backorders",Yes,No,No,Yes,Yes,Yes,No,No,,,No,Yes,20.0000,0.0000,0.0000,1000.0000,,0.0000,,,,,20.0000,20.0000,20.0000'."\n");
                    }
                    fclose($file);
                }
            }
            else {
                if(!file_exists($this->_cacheFile.$test_num.'.csv')) {
                    $file = fopen($this->_cacheFile.$test_num.'.csv','w');
                    fwrite($file, 'color,cost,country_of_manufacture,created_at,custom_design,custom_design_from,custom_design_to,custom_layout_update,description,enable_googlecheckout,gift_message_available,group_price,image,image_label,is_recurring,manufacturer,meta_description,meta_keyword,meta_title,msrp,msrp_display_actual_price_type,msrp_enabled,name,news_from_date,news_to_date,old_id,options_container,page_layout,price,price_view,recurring_profile,short_description,sku,small_image,small_image_label,special_from_date,special_price,special_to_date,status,tax_class_id,thumbnail,thumbnail_label,tier_price,updated_at,url_key,url_path,visibility,weight,product.attribute_set,product.type,product.websites,category.ids,category.path,category.name,product.has_options,product.required_options,stock.is_in_stock,stock.backorders,stock.manage_stock,stock.use_config_manage_stock,stock.is_qty_decimal,stock.use_config_notify_stock_qty,stock.use_config_min_qty,stock.use_config_backorders,stock.use_config_min_sale_qty,stock.use_config_max_sale_qty,stock.stock_status_changed_automatically,stock.use_config_enable_qty_increments,stock.enable_qty_increments,stock.use_config_qty_increments,stock.qty,stock.min_qty,stock.min_sale_qty,stock.max_sale_qty,stock.notify_stock_qty,stock.qty_increments,price_type,weight_type,sku_type,shipment_type,price.final,price.minimal,price.maximum'."\n");
                    for($i = 0; $i < $test_num; $i++) {
                        fwrite($file, ',,,"2013-10-01 14:58:53",,,,,"Das ist ein Test '.$i.'",Yes,,,,,,,,,,,,,"Test '.$i.'",,,,"Block after Info Column",,20.0000,,,"Testprodukt '.$i.'",importtest'.$i.',,,,,,Enabled,None,,,,"2013-10-02 10:07:16",test-'.$i.',test-'.$i.'.html,"Catalog, Search",4.0000,Default,simple,base,,,,No,No,"In Stock","No Backorders",Yes,No,No,Yes,Yes,Yes,No,No,,,No,Yes,20.0000,0.0000,0.0000,1000.0000,,0.0000,,,,,20.0000,20.0000,20.0000'."\n");
                    }
                    fclose($file);
                }
            }
        }
    }

}
