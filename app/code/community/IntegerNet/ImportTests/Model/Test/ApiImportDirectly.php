<?php

/**
 * @category   IntegerNet
 * @package    IntergerNet_ImportTests
 * @license    http://opensource.org/licenses/osl-3.0.php Open Software Licence 3.0 (OSL-3.0)
 * @author     Soeren Zorn <sz@integer-net.de>
 */

class IntegerNet_ImportTests_Model_Test_ApiImportDirectly extends IntegerNet_ImportTests_Model_Test
{
	/**
	 * Run tests
	 *
	 * @return float[]
	 */
	public function runTests()
	{
        $results = array();
        
        //Some Modules overwrite default import config -> Load correct config:
        $this->_load_correct_config('Danslo_ApiImport');

        $apiImport = Mage::getModel('api_import/import_api');

        $importData = $this->_generate_data();

		foreach($this->getRuns() as $run)
		{
			$this->deleteAddedProducts();

			$time = microtime(true);

            $apiImport->importEntities($importData[$run]);

			$results[] = round(microtime(true) - $time, 2);
		}
		return $results;
	}

    protected function _generate_data()
    {
        $arraySkeleton = array(
            '_type' => 'simple',
            '_attribute_set' => 'Default',
            '_product_websites' => 'base',
            'weight' => 4,
            'status' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED,
            'visibility' => Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE,
            'tax_class_id' => 0,
            'price' => 20,
            'has_options' => 0,
            'website_id' => 1,

            'qty' => 20,
            'is_in_stock' => 1,
            'stock_id' => 1,
            'store_id' => 1,
            'manage_stock' => 1,
            'use_config_manage_stock' => 1,
            'use_config_min_sale_qty' => 1,
            'use_config_max_sale_qty' => 1,
        );
        $importData = array();
        foreach($this->getRuns() as $run) {
            $importData[$run] = array();
            for ($i = 0; $i < $run; $i++)
            {
                if($this->_productType == 'configurable') {
                    $importData[$run][] = array_merge($arraySkeleton, array(
                        'sku' => 'importtest-configurable-Small-'.$i,
                        'name' => 'ImportTest Configurable-Small ' . $i,
                        'description' => 'Das ist ein Test ' . $i,
                        'short_description' => 'Testprodukt ' . $i,
                        'importtest_configurable' => 'Small',
                    ));

                    $importData[$run][] = array_merge($arraySkeleton, array(
                        'sku' => 'importtest-configurable-Medium-'.$i,
                        'name' => 'ImportTest Configurable-Medium ' . $i,
                        'description' => 'Das ist ein Test ' . $i,
                        'short_description' => 'Testprodukt ' . $i,
                        'importtest_configurable' => 'Medium',
                    ));

                    $importData[$run][] = array_merge($arraySkeleton, array(
                        'sku' => 'importtest-configurable-Large-'.$i,
                        'name' => 'ImportTest Configurable-Large ' . $i,
                        'description' => 'Das ist ein Test ' . $i,
                        'short_description' => 'Testprodukt ' . $i,
                        'importtest_configurable' => 'Large',
                    ));

                    $importData[$run][] = array_merge($arraySkeleton, array(
                        'sku' => 'importtest-configurable-XLarge-'.$i,
                        'name' => 'ImportTest Configurable-XLarge ' . $i,
                        'description' => 'Das ist ein Test ' . $i,
                        'short_description' => 'Testprodukt ' . $i,
                        'importtest_configurable' => 'XLarge',
                    ));

                    $importData[$run][] = array_merge($arraySkeleton, array(
                        '_type' => 'configurable',
                        'sku' => 'importtest-configurable-'.$i,
                        'name' => 'ImportTest Configurable ' . $i,
                        'description' => 'Das ist ein Test ' . $i,
                        'short_description' => 'Testprodukt ' . $i,
                        'visibility' => Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
                        'has_options' => 1,
                        '_super_products_sku' => 'importtest-configurable-Small-'.$i,
                        '_super_attribute_code' => 'importtest_configurable',
                        '_super_attribute_option' => 'Small',
                    ));

                    $importData[$run][] = array(
                        '_super_products_sku' => 'importtest-configurable-Medium-'.$i,
                        '_super_attribute_code' => 'importtest_configurable',
                        '_super_attribute_option' => 'Medium',
                    );
                    $importData[$run][] = array(
                        '_super_products_sku' => 'importtest-configurable-Large-'.$i,
                        '_super_attribute_code' => 'importtest_configurable',
                        '_super_attribute_option' => 'Large',
                    );
                    $importData[$run][] = array(
                        '_super_products_sku' => 'importtest-configurable-XLarge-'.$i,
                        '_super_attribute_code' => 'importtest_configurable',
                        '_super_attribute_option' => 'XLarge',
                    );
                }
                else {
                    $importData[$run][] = array(
                        '_attribute_set' => 'Default',
                        '_type' => 'simple',
                        '_product_websites' => 'base',
                        'sku' => 'importtest' . $i,
                        'name' => 'Test ' . $i,
                        'description' => 'Das ist ein Test ' . $i,
                        'short_description' => 'Testprodukt ' . $i,
                        'weight' => 4,
                        'status' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED,
                        'visibility' => Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
                        'tax_class_id' => 0,
                        'price' => 20,

                        'qty' => 20,
                        'is_in_stock' => 1,
                        'stock_id' => 1,
                        'store_id' => 1,
                        'manage_stock' => 1,
                        'use_config_manage_stock' => 1,
                        'use_config_min_sale_qty' => 1,
                        'use_config_max_sale_qty' => 1,
                    );
                }
            }
        }
        return $importData;
    }

}
