<?php

/**
 * @category   IntegerNet
 * @package    IntergerNet_ImportTests
 * @license    http://opensource.org/licenses/osl-3.0.php Open Software Licence 3.0 (OSL-3.0)
 * @author     Andreas von Studnitz <avs@integer-net.de>
 */

class IntegerNet_ImportTests_Model_Test_ExIm extends IntegerNet_ImportTests_Model_Test
{

    public function checkPrerequisites()
    {
        $profile = Mage::getModel('ecomdev_exim/profile');
        if (!$profile) {
            Mage::throwException('The module EcomDev_ExIm is not installed.');
        }
        $profile->load('importtest', 'code');
        if (!$profile->getId()) {
            Mage::throwException('Please create a profile with the code "importtest" before.');
        }
    }
	/**
	 * Run tests
	 *
	 * @return float[]
	 */
	public function runTests()
	{
        $results = array();

        $this->_generate_data();

		foreach($this->getRuns() as $run)
		{
			$this->deleteAddedProducts();

            $dir = Mage::getBaseDir().DS.'var'.DS.'exim'.DS.'import'.DS;
            if(!is_dir($dir))
            {
                mkdir($dir);
            }
            copy($this->_cacheFile.$run.'.csv', $dir .'data.csv');

            $time = microtime(true);

            Mage::getModel('ecomdev_exim/profile')
                ->load('importtest', 'code')
                ->setCronEnabled(true)
                ->run();

			$results[] = round(microtime(true) - $time, 2);
		}
		return $results;
	}

    protected function _generate_data()
    {
        // Create Export file once
        foreach ($this->getRuns() as $test_num)
        {
            if($this->_productType == 'configurable') {
                if(!file_exists($this->_cacheFile.$test_num.'.csv')) {
                    $file = fopen($this->_cacheFile.$test_num.'.csv','w');
                    fwrite($file, 'sku;configurable_product_sku;websites;product_type;price;cost;stock.is_in_stock;stock.qty;status;visibility;attributeset;importtest_configurable;configurable_product_options;options_container;tax_class_id;weight;name;description;short_description'."\n");
                    for($i = 0; $i < $test_num; $i++) {
                        fwrite($file, 'importtest-' . $i . ';;base;configurable;1.45;;1;0;Enabled;Catalog, Search;Default;;importtest_configurable;Block after Info Column;2;0.06;Test ' . $i . ';;'."\n");
                        fwrite($file, 'importtest-' . $i . '-1;importtest-' . $i . ';base;simple;1.45;;1;20;Enabled;Not Visible Individually;Default;Small;;Block after Info Column;2;0.06;Test ' . $i . ' 1;;'."\n");
                        fwrite($file, 'importtest-' . $i . '-2;importtest-' . $i . ';base;simple;1.45;;1;20;Enabled;Not Visible Individually;Default;Medium;;Block after Info Column;2;0.06;Test ' . $i . ' 2;;'."\n");
                        fwrite($file, 'importtest-' . $i . '-3;importtest-' . $i . ';base;simple;1.45;;1;20;Enabled;Not Visible Individually;Default;Large;;Block after Info Column;2;0.06;Test ' . $i . ' 3;;'."\n");
                        fwrite($file, 'importtest-' . $i . '-4;importtest-' . $i . ';base;simple;1.45;;1;20;Enabled;Not Visible Individually;Default;XLarge;;Block after Info Column;2;0.06;Test ' . $i . ' 4;;'."\n");
                    }
                    fclose($file);
                }
            }
            else {
                if(!file_exists($this->_cacheFile.$test_num.'.csv')) {
                    $file = fopen($this->_cacheFile.$test_num.'.csv','w');
                    fwrite($file, 'sku;websites;product_type;price;cost;stock.is_in_stock;stock.qty;status;visibility;attributeset;options_container;tax_class_id;weight;name;description;short_description'."\n");
                    for($i = 0; $i < $test_num; $i++) {
                        fwrite($file, 'importtest-' . $i . ';base;simple;4.71;;0;1069;Enabled;Catalog, Search;Default;Block after Info Column;2;0.06;Test ' . $i . ';Test;Test'."\n");
                    }
                    fclose($file);
                }
            }
        }
    }

}
