<?php

/**
 * @category   IntegerNet
 * @package    IntergerNet_ImportTests
 * @license    http://opensource.org/licenses/osl-3.0.php Open Software Licence 3.0 (OSL-3.0)
 * @author     Soeren Zorn <sz@integer-net.de>
 */

class IntegerNet_ImportTests_Model_Test_CustomImportExport extends IntegerNet_ImportTests_Model_Test
{
	/**
	 * Run tests
	 *
	 * @return float[]
	 */
	public function runTests()
	{
        $results = array();
        
        //Some Modules overwrite default import config -> Load correct config:
        $this->_load_correct_config('AMartinez_CustomImportExport');

        $this->_generate_data();

		foreach($this->getRuns() as $run)
		{
			$this->deleteAddedProducts();

			$time = microtime(true);
            $dir = Mage::getBaseDir() . DS . 'var' . DS . 'customimportexport' . DS;
            if(!is_dir($dir))
            {
                mkdir($dir);
            }
			copy($this->_cacheFile.$run.'.csv', $dir .'catalog_product.csv');

			$buffer = shell_exec('php '.Mage::getBaseDir().DS.'amartinez_customimportexport.php -all -import '.$dir.'catalog_product.csv');
            echo $buffer;

			$results[] = round(microtime(true) - $time, 2);
		}
		return $results;
	}

    protected function _generate_data()
    {
        // Create Export file once
        foreach ($this->getRuns() as $test_num)
        {
            if(!file_exists($this->_cacheFile.$test_num.'.csv'))
            {
                $file = fopen($this->_cacheFile.$test_num.'.csv','w');
                fwrite($file, 'sku,_store,_attribute_set,_type,_category,_product_websites,color,cost,country_of_manufacture,created_at,custom_design,custom_design_from,custom_design_to,custom_layout_update,description,enable_googlecheckout,gallery,gift_message_available,has_options,image,image_label,manufacturer,media_gallery,meta_description,meta_keyword,meta_title,minimal_price,msrp,msrp_display_actual_price_type,msrp_enabled,name,news_from_date,news_to_date,options_container,page_layout,price,required_options,short_description,small_image,small_image_label,special_from_date,special_price,special_to_date,status,tax_class_id,thumbnail,thumbnail_label,updated_at,url_key,url_path,visibility,weight,qty,min_qty,use_config_min_qty,is_qty_decimal,backorders,use_config_backorders,min_sale_qty,use_config_min_sale_qty,max_sale_qty,use_config_max_sale_qty,is_in_stock,notify_stock_qty,use_config_notify_stock_qty,manage_stock,use_config_manage_stock,stock_status_changed_auto,use_config_qty_increments,qty_increments,use_config_enable_qty_inc,enable_qty_increments,is_decimal_divided,_links_related_sku,_links_related_position,_links_crosssell_sku,_links_crosssell_position,_links_upsell_sku,_links_upsell_position,_associated_sku,_associated_default_qty,_associated_position'."\n");
                for($i = 0; $i < $test_num; $i++)
                {
                    fwrite($file, 'customimporttest'.$i.',,Default,simple,,,,,,"2013-10-08 09:08:36",,,,,"Das ist ein Test '.$i.'",1,,,'.$i.',no_selection,,,,,,,,,,,"Test '.$i.'",,,"Block after Info Column",,20.0000,0,"Testprodukt '.$i.'",no_selection,,,,,1,0,no_selection,,"2013-10-08 09:08:36",test-'.$i.',,4,4.0000,20.0000,0.0000,1,0,0,1,0.0000,0,1000.0000,0,1,,1,1,0,0,1,0.0000,1,0,0,,,,,,,,,'."\n");
                }
                fclose($file);
            }
        }
    }

}
