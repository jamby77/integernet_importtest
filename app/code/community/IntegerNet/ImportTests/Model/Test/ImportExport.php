<?php

/**
 * @category   IntegerNet
 * @package    IntergerNet_ImportTests
 * @license    http://opensource.org/licenses/osl-3.0.php Open Software Licence 3.0 (OSL-3.0)
 * @author     Soeren Zorn <sz@integer-net.de>
 */

class IntegerNet_ImportTests_Model_Test_ImportExport extends IntegerNet_ImportTests_Model_Test
{
	/**
	 * Run tests
	 *
	 * @return float[]
	 */
	public function runTests()
	{
        $results = array();

        $this->_generate_data();

        //Some Modules overwrite default import config -> Load correct config:
        $this->_load_correct_config('Mage_ImportExport');

		$data = array(
			'entity' => 'catalog_product',
			'behavior' => Mage_ImportExport_Model_Import::BEHAVIOR_APPEND,
		);
		foreach($this->getRuns() as $run)
		{
			$this->deleteAddedProducts();

			$time = microtime(true);
			$import = Mage::getModel('importexport/import');
			$import->setData($data);
            $dir = Mage::getBaseDir() . DS . 'var' . DS . 'importexport' . DS;
            if(!is_dir($dir))
            {
                mkdir($dir);
            }
			copy($this->_cacheFile.$run.'.csv', $dir .'catalog_product.csv');
			$validationResult = $import->validateSource($dir .'catalog_product.csv');
			try {
				$import->importSource();
				$import->invalidateIndex();
			} catch (Exception $e) {
				echo 'Error Import Source';
			}

			$results[] = round(microtime(true) - $time, 2);
		}
		return $results;
	}

    protected function _generate_data()
    {
        // Create Export file once
        foreach ($this->getRuns() as $test_num)
        {
            if($this->_productType == 'configurable') {
                if(!file_exists($this->_cacheFile.$test_num.'.csv')) {
                    $file = fopen($this->_cacheFile.$test_num.'.csv','w');
                    fwrite($file, 'sku,_store,_attribute_set,_type,_category,_root_category,_product_websites,description,has_options,importtest_configurable,name,options_container,price,required_options,short_description,status,tax_class_id,updated_at,visibility,weight,qty,min_qty,use_config_min_qty,is_qty_decimal,backorders,use_config_backorders,min_sale_qty,use_config_min_sale_qty,max_sale_qty,use_config_max_sale_qty,is_in_stock,notify_stock_qty,use_config_notify_stock_qty,manage_stock,use_config_manage_stock,stock_status_changed_auto,use_config_qty_increments,qty_increments,use_config_enable_qty_inc,enable_qty_increments,is_decimal_divided,_links_related_sku,_links_related_position,_links_crosssell_sku,_links_crosssell_position,_links_upsell_sku,_links_upsell_position,_associated_sku,_associated_default_qty,_associated_position,_tier_price_website,_tier_price_customer_group,_tier_price_qty,_tier_price_price,_group_price_website,_group_price_customer_group,_group_price_price,_media_attribute_id,_media_image,_media_lable,_media_position,_media_is_disabled,_super_products_sku,_super_attribute_code,_super_attribute_option,_super_attribute_price_corr'."\n");
                    for($i = 0; $i < $test_num; $i++)
                    {
                        fwrite($file, 'importtest-configurable-Small-'.$i.',,Default,simple,,,base,"Product that can be configured",0,Small,"ImportTest Configurable-Small '.$i.'","Block after Info Column",20.0000,0,"Product that can be configured",1,0,"2013-10-09 15:27:27",1,3.0000,2.0000,0.0000,1,0,0,1,1.0000,1,0.0000,1,1,,1,0,1,0,1,0.0000,1,0,0,,,,,,,,,,,,,,,,,,,,,,,,,'."\n");
                        fwrite($file, 'importtest-configurable-Medium-'.$i.',,Default,simple,,,base,"Product that can be configured",0,Medium,"ImportTest Configurable-Medium '.$i.'","Block after Info Column",20.0000,0,"Product that can be configured",1,0,"2013-10-09 15:27:44",1,3.0000,2.0000,0.0000,1,0,0,1,1.0000,1,0.0000,1,1,,1,0,1,0,1,0.0000,1,0,0,,,,,,,,,,,,,,,,,,,,,,,,,'."\n");
                        fwrite($file, 'importtest-configurable-Large-'.$i.',,Default,simple,,,base,"Product that can be configured",0,Large,"ImportTest Configurable-Large '.$i.'","Block after Info Column",20.0000,0,"Product that can be configured",1,0,"2013-10-09 15:27:58",1,3.0000,2.0000,0.0000,1,0,0,1,1.0000,1,0.0000,1,1,,1,0,1,0,1,0.0000,1,0,0,,,,,,,,,,,,,,,,,,,,,,,,,'."\n");
                        fwrite($file, 'importtest-configurable-XLarge-'.$i.',,Default,simple,,,base,"Product that can be configured",0,XLarge,"ImportTest Configurable-XLarge '.$i.'","Block after Info Column",20.0000,0,"Product that can be configured",1,0,"2013-10-09 15:28:10",1,3.0000,2.0000,0.0000,1,0,0,1,1.0000,1,0.0000,1,1,,1,0,1,0,1,0.0000,1,0,0,,,,,,,,,,,,,,,,,,,,,,,,,'."\n");
                        fwrite($file, 'importtest-configurable-'.$i.',,Default,configurable,,,base,"Product that can be configured",1,,"ImportTest Configurable '.$i.'","Block after Info Column",20.0000,1,"Product that can be configured",1,0,"2013-10-09 15:28:28",4,,0.0000,0.0000,1,0,0,1,1.0000,1,0.0000,1,1,,1,0,1,0,1,0.0000,1,0,0,,,,,,,,,,,,,,,,,,,,,,importtest-configurable-Small-'.$i.',importtest_configurable,Small,'."\n");
                        fwrite($file, ',,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,importtest-configurable-Medium-'.$i.',importtest_configurable,Medium,'."\n");
                        fwrite($file, ',,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,importtest-configurable-Large-'.$i.',importtest_configurable,Large,'."\n");
                        fwrite($file, ',,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,importtest-configurable-XLarge-'.$i.',importtest_configurable,XLarge,'."\n");
                    }
                }
            }
            else {
                if(!file_exists($this->_cacheFile.$test_num.'.csv')) {
                    $file = fopen($this->_cacheFile.$test_num.'.csv','w');
                    fwrite($file, 'sku,_store,_attribute_set,_type,_category,_root_category,_product_websites,description,name,price,short_description,status,tax_class_id,visibility,weight,qty,min_qty,use_config_min_qty,is_qty_decimal,backorders,use_config_backorders,min_sale_qty,use_config_min_sale_qty,max_sale_qty,use_config_max_sale_qty,is_in_stock,notify_stock_qty,use_config_notify_stock_qty,manage_stock,use_config_manage_stock,stock_status_changed_auto,use_config_qty_increments,qty_increments,use_config_enable_qty_inc,enable_qty_increments,is_decimal_divided,_links_related_sku,_links_related_position,_links_crosssell_sku,_links_crosssell_position,_links_upsell_sku,_links_upsell_position,_associated_sku,_associated_default_qty,_associated_position,_tier_price_website,_tier_price_customer_group,_tier_price_qty,_tier_price_price,_group_price_website,_group_price_customer_group,_group_price_price,_media_attribute_id,_media_image,_media_lable,_media_position,_media_is_disabled'."\n");
                    for($i = 0; $i < $test_num; $i++) {
                        fwrite($file, 'importtest'.$i.',,Default,simple,,,base,"Das ist ein Test '.$i.'","Test '.$i.'",20.0000,"Testprodukt '.$i.'",1,0,4,4.0000,20.0000,0.0000,1,0,0,1,0.0000,0,1000.0000,0,1,,1,1,0,0,1,0.0000,1,0,0,,,,,,,,,,,,,,,,,,,,,'."\n");
                    }
                    fclose($file);
                }
            }
        }
    }

}
