<?php

/**
 * @category   IntegerNet
 * @package    IntergerNet_ImportTests
 * @license    http://opensource.org/licenses/osl-3.0.php Open Software Licence 3.0 (OSL-3.0)
 * @author     Soeren Zorn <sz@integer-net.de>
 */

abstract class IntegerNet_ImportTests_Model_Test extends Mage_Core_Model_Abstract
{
    protected $_data = array(
        'runs' => array(
            10,
            100,
            1000,
            10000,
        ),
    );

    protected $_results = array();

    protected $_cacheFile;
    protected $_productType = 'simple';

    public function __construct($arguments = array())
    {
        if(isset($arguments['productType'])) {
            $this->_productType = $arguments['productType'];
        }
        $cacheDir = Mage::getBaseDir('var').DS.'importtestcache/';
        if(!is_dir($cacheDir)) {
            mkdir($cacheDir);
        }
        if($this->_productType == 'configurable') {
            $this->_cacheFile = $cacheDir.strtolower(str_replace('IntegerNet_ImportTests_Model_Test_', '', get_class($this))).'-configurable-';
        }
        else {
            $this->_cacheFile = $cacheDir.strtolower(str_replace('IntegerNet_ImportTests_Model_Test_', '', get_class($this))).'-';
        }
    }

    abstract protected function _generate_data();

    protected function _load_correct_config($moduleName)
    {
        $config = Mage::getConfig();
        $prototype = new Mage_Core_Model_Config_Base();
        $isConfigLoaded = $prototype->loadFile(Mage::getModuleDir('etc',$moduleName).DS.'config.xml');
        if($isConfigLoaded) {
            $config->extend($prototype, true);
        }
        $mergeConfig = new Mage_Core_Model_Config_Base();
        $isLocalConfigLoaded = $mergeConfig->loadFile(Mage::getBaseDir('etc').DS.'local.xml');
        if ($isLocalConfigLoaded) {
            $config->extend($mergeConfig);
        }
        $config->applyExtends();
    }

    /**
     * Delete all added products
     *
     * @return IntegerNet_ImportTests_Model_Test
     */
    public function deleteAddedProducts()
    {
        $resource = Mage::getSingleton('core/resource');
        $write = $resource->getConnection(Mage_Core_Model_Resource::DEFAULT_SETUP_RESOURCE);
        //$write->query("DELETE FROM catalog_product_entity WHERE sku LIKE 'importtest%'");
        $write->query("TRUNCATE TABLE catalog_product_entity");
        $collection = Mage::getModel('index/process')->getCollection();
        foreach ($collection as $item) {
            $item->reindexAll();
        }
        return $this;
    }

    /**
     * Run tests
     *
     * @return IntegerNet_ImportTests_Model_Test
     */
    abstract public function runTests();

    public function checkPrerequisites()
    {

    }
}
