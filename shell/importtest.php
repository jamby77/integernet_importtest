<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Shell
 * @copyright   Copyright (c) 2009 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

require_once 'abstract.php';

/**
 * IntegerNet Compiler Shell Script
 *
 * @category    IntegerNet
 * @package     IntegerNet_Shell
 * @author      Soeren Zorn <sz@integer-net.de>
 */
class IntegerNet_Shell_Importtest extends Mage_Shell_Abstract
{

    /**
     * Run script
     *
     */
    public function run()
    {
        if($apiUser = $this->getArg('ApiUser'))
        {
            $setup = Mage::getModel('eav/entity_setup', 'core_setup');
            $setup->setConfigData('ImportTest/ApiUser', $apiUser);
            echo "\nSaved ApiUser\n";
        }
        elseif($apiKey = $this->getArg('ApiKey'))
        {
            $setup = Mage::getModel('eav/entity_setup', 'core_setup');
            $setup->setConfigData('ImportTest/ApiKey', $apiKey);
            echo "\nSaved ApiKey\n";
        }
        // Intern requests
        elseif(($batchId = $this->getArg('dataFlowBatch')) && ($rowIds = explode(',',$this->getArg('rows'))))
        {
            /** @var IntegerNet_ImportTests_Model_Test_DataFlow $dataFlowModel */
            $dataFlowModel = Mage::getModel('importtests/test_dataFlow');
            $dataFlowModel->runBatch($batchId, $rowIds);
        }
        elseif($batchId = $this->getArg('dataFlowFinish'))
        {
            /** @var IntegerNet_ImportTests_Model_Test_DataFlow $dataFlowModel */
            $dataFlowModel = Mage::getModel('importtests/test_dataFlow');
            $dataFlowModel->batchFinish($batchId);
        }
        //external requests
        elseif($this->getArg('test'))
        {
            $time = microtime(true);

            $productType = ($this->getArg('type') == 'configurable') ? 'configurable' : 'simple';
            $runs = array(
                10,
                100,
                1000,
                10000
            );
            // $test = Mage::getModel('customimportexport/import_entity_product');
            /** @var IntegerNet_ImportTests_Model_Test[] $tests */
            $tests = array();
            $testNames = array();
            $types = explode(',',$this->getArg('test'));
            foreach($types as $type) {
                switch ($type) {
                    case 'Mage':
                        $tests[] = Mage::getModel('importtests/test_mage', array('productType' => $productType));
                        $testNames[] = 'Mage';
                        break;
                    case 'FastSimpleImport':
                        $tests[] = Mage::getModel('importtests/test_fastSimpleImport', array('productType' => $productType));
                        $testNames[] = 'FastSimpleImport';
                        break;
                    case 'ImportExport':
                        $test = Mage::getModel('importtests/test_importExport', array('productType' => $productType));
                        $tests[] = $test;
                        $testNames[] = 'ImportExport';
                        break;
                    case 'Magmi':
                        $tests[] = Mage::getModel('importtests/test_magmi', array('productType' => $productType));
                        $testNames[] = 'Magmi';
                        break;
                    case 'Soap':
                        $tests[] = Mage::getModel('importtests/test_soap', array('productType' => $productType));
                        $testNames[] = 'Soap';
                        break;
                    case 'AmartinezCustomImportExport':
                        $tests[] = Mage::getModel('importtests/test_customImportExport', array('productType' => $productType));
                        $testNames[] = 'Amartinez CustomImportExport';
                        break;
                    case 'URapidFlow':
                        $tests[] = Mage::getModel('importtests/test_uRapidFlow', array('productType' => $productType));
                        $testNames[] = 'uRapidFlow';
                        break;
                    case 'EcomDev_ExIm':
                        $tests[] = Mage::getModel('importtests/test_exIm', array('productType' => $productType));
                        $testNames[] = 'EcomDev_ExIm';
                        break;
                    case 'Rest':
                        $tests[] = Mage::getModel('importtests/test_rest', array('productType' => $productType));
                        $testNames[] = 'Rest';
                        break;
                    case 'ApiImport':
                    case 'ApiImportDirectly':
                        $tests[] = Mage::getModel('importtests/test_apiImportDirectly', array('productType' => $productType));
                        $testNames[] = 'ApiImport Direct';
                        break;
                    case 'ApiImportXmlrpc':
                        $tests[] = Mage::getModel('importtests/test_apiImportXmlrpc', array('productType' => $productType));
                        $testNames[] = 'ApiImport XMLRPC';
                        break;
                    case 'DataFlow':
                        /** @var IntegerNet_ImportTests_Model_Test_DataFlow $dataFlowModel */
                        $dataFlowModel = Mage::getModel('importtests/test_dataFlow', array('productType' => $productType));
                        $chunkSize = 1;
                        if($this->getArg('chunksize'))
                        {
                            $chunkSize = $this->getArg('chunksize');
                        }
                        else
                        {
                            echo "\nUsing DataFlow you may want to set -chunksize <value> to specify how many Products should be added at once. Default: 1\n";
                        }
                        $dataFlowModel->setChunkSize($chunkSize);
                        $tests[] = $dataFlowModel;
                        $testNames[] = 'DataFlow';
                        break;
                    default:
                        echo "\nPlease select an available test\n";
                        exit;
                }
            }
            if($arg = $this->getArg('noruns')) {
                $runs = explode(',', $arg);
            }

            // Check
            $errors = array();
            foreach($tests as $key => $test) {
                try {
                    $test->setRuns(array(1))->checkPrerequisites();
                } catch (Exception $e) {
                    $errors[$testNames[$key]] = $e->getMessage();
                }
            }
            if (sizeof($errors)) {
                foreach($errors as $testName => $errormessage) {
                    echo $testName . ': ';
                    echo $errormessage . "\n";
                }
                return;
            }

            // Do import
            $results = array();
            foreach($tests as $test) {
                $test->setRuns($runs);
                $results[] = $test->runTests();
            }

            echo "\n\n";
            // Display header
            echo sprintf('%-30s', 'Runs');
            foreach ($runs as $run) {
                echo sprintf('%-8s', $run);
            }
            echo "\n";
            echo '-----------------------------------------------------------------------';
            echo "\n";

            // Display results
            foreach($testNames as $key => $testName) {
                echo sprintf('%-30s', $testName);
                foreach ($results[$key] as $result) {
                    echo sprintf('%-8s', $result.'s');
                }
                echo "\n";
            }
            echo "\n\n";
            $elapsedTime = round(microtime(true) - $time, 2);
            echo "Total time elapsed: {$elapsedTime}s.\n\n";
        }
        elseif($this->getArg('info'))
        {
            $availableTests = array('Mage', 'FastSimpleImport', 'ImportExport', 'Magmi', 'Soap', 'URapidFlow', 'ApiImportDirectly', 'ApiImportXmlrpc', 'EcomDev_ExIm', 'DataFlow', 'Rest', 'AmartinezCustomImportExport');
            echo implode(', ',$availableTests)."\n";
        }
        else
        {
            echo $this->usageHelp();
        }
    }

    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php -f importtest.php -- [options]

  -test <test>            Choose type of test
  -noruns <runs>          Set comma seperate list of number of runs
  -type <productType>     Set product type to either simple or configurable, defaults to simple
  -info                   List available tests

USAGE;
    }
}

$shell = new IntegerNet_Shell_Importtest();
$shell->run();
